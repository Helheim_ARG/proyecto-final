Trabajo Final 
    AHORCADO

Materia:
    Programacion II

Profesor:
    Gracia Jorge

Integrantes:
    Torres Olivera Luis Nicolas (Tester y Documentacion)
    De la Vega Leonel (lider)
    Olivera Fernando (Diseño, programacion)

Librerias:
    SQlite3
    python
    pyQT5

Instructivo: 

    Iniciamos el juego nos aparecera en la primera pantalla donde nos permitira colocar nuestro nombre de usuario
    o usar un usuario existente, podremos tambien cambiar la dificultad con la cual jugaremos y la categoria, o simplemente
    ver el ranking.

    la dificultad solo hace variar la longitud y complejidad de las palabras 

    Una vez hecho esto entraremos a la pantalla del juego, veremos un teclado y arriba lineas que simbolizaran
    los espacios que deberemos llenar correctamente para descubrir la palabra, en el sector izquierdo de la pantalla
    podremos ver los puntos que vayamos logrando mientras jugamos y a la derecha veremos nuestras vidas.
    Una vez que presionemos una tecla si es correcta saldra en verde y rellenara los espacios correspondientes
    con la letra acertada y veremos los puntos ganados, de ser incorrecta la letra quedara en rojo, se nos descontara
    una vida pero podremos seguir jugando. 
    De poder descubrir la palabra correctamente se reiniciara el teclado, y apareceran nuevos espacios para llenar 
    pero con las vidas que aun nos queden. 


