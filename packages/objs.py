from PyQt5.QtWidgets import QPushButton
from PyQt5 import QtCore
from PyQt5.QtGui import QCursor


def fnameToCss(k):
    ret = k[0]
    for x in k[1:]:
        if x.isupper():
            ret += '-' + x.lower()
        else:
            ret += x
    return ret


def cssToFname(k):
    c = 0
    ret = ''
    isMay = False
    while c < len(k):
        if k[c] != '-' and not isMay:
            ret += k[c]
        elif k[c] != '-':
            ret += k[c].upper()
            isMay = False
        else:
            isMay = True
        c += 1
    return ret


def getCSS(**args):
    style = {}
    for k, v in args.items():
        style[fnameToCss(k)] = v
    ret = '\n'.join(k+':'+str(v)+';' for k, v in style.items())
    return ret


class Key(QPushButton):  # Botones del keyboard
    def __init__(self, value, name, clicked=lambda x: x):
        super().__init__(
                         objectName=name.replace('ñ', 'nn').upper(),
                         clicked=clicked
                         )
        self.style = {}
        self.css(
                    backgroundImage='url(./src/img/graybgb.png)',
                    color='#747171',
                    backgroundColor='transparent',
                    backgroundRepeat='no-repeat',
                    maxWidth='64px',
                    maxHeight='64px',
                    borderStyle='none',
                    fontSize='30px',
        )
        self.setFixedSize(64, 64)
        self.setText(value.replace('nn', 'ñ').upper())
        self.__signValue__ = value.replace('nn', 'ñ')
        self.setCursor(QCursor(QtCore.Qt.PointingHandCursor))

    def getValue(self):
        return self.__signValue__

    def css(self, **args):
        for k, v in args.items():
            self.style[fnameToCss(k)] = v
        ret = '\n'.join(k+':'+str(v)+';' for k, v in self.style.items())
        self.setStyleSheet(ret)

    def onKeyPress(self, inp):
        color = 'green' if inp else 'red'
        self.css(
                    color='#ffffff',
                    backgroundImage=f'url(./src/img/{color}bgb.png)'
                )
        self.setCursor(QCursor(QtCore.Qt.ArrowCursor))
        self.repaint()

    def reset(self):
        self.css(
                backgroundImage='url(./src/img/graybgb.png)',
                color='#747171',
        )
        self.setCursor(QCursor(QtCore.Qt.PointingHandCursor))


class Toggle(QPushButton):
    def __init__(self, title='', options=[]):
        super().__init__()
        self.default = 0
        self.refe = options
        self.title = title if type(title) == str else ''
        self.options = options if type(
            options) == list else list(options.keys())
        self.__value__ = self.options[0] if len(
            self.options) > 1 else ''
        self.updateText()
        self.setStyleSheet('QPushButton{'+getCSS(
            backgroundImage='url(./src/img/greenbgxl.png)',
            backgroundRepeat='repeat-x',
            backgroundPosition='center center',
            borderRadius='9px',
            color='white',
            fontSize='18px',
            minHeight='40px'
        )+'} QPushButton:hover{background-image:url(./src/img/greenbgxl-hover.png)}')
        self.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.clicked.connect(self.changeState)

    def changeState(self):
        self.default = (self.default + 1) % len(self.options)
        self.__value__ = self.options[self.default]
        self.updateText()

    def updateText(self):
        if self.default in range(0, len(self.options)):
            self.setText(
                f'{self.title}:{self.options[self.default].rjust(25-len(self.title))}')
        self.repaint()

    def getValue(self):
        return self.__value__ if type(self.refe) == list else self.refe[self.__value__]
