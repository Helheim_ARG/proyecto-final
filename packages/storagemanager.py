import json


def readJSON(dir):
    try:
        ret = json.loads(open(dir, 'r', encoding='utf8').read())
    except:
        ret = False
    return ret


def writeJSON(dir, data):
    try:
        file = open(dir, 'w+', encoding='utf8')
        file.write(json.dumps(data, ensure_ascii=False))
        file.close()
    except:
        file = False
    return file


class dbGame:
    def __init__(self, dataset=[], dataDir='./data/data.json', prefDir='./data/pref.json'):
        self.__prefDir__ = prefDir
        self.__dataDir__ = dataDir
        self.__data__ = readJSON(self.__dataDir__) or {
            'users': [],
            'tableData': [],
            'tableHeaders': [
                                'player', 'dificultad', 'score', 'words',
                                'lengthWords', 'startTime', 'endTime', 'time'
                            ]
        }
        self.__pref__ = readJSON(self.__prefDir__) or {
            'defaultUser': ''
        }
        if type(dataset) == list and dataset != []:
            for x in dataset:
                if self.__validEntry__(x):
                    obj = self.__prepareEntry__(x)
                    self.__data__['tableData'].append(obj)
                    if obj[0] not in self.__data__['users']:
                        self.__data__['users'].append(obj[0])
                        self.__pref__['defaultUser'] = obj[0]
        self.__update__()

    def getRawData(self):
        return self.__data__

    def getRawPref(self):
        return self.__data__

    def getRows(self, includeTags=False):
        mdata = sorted(self.__data__['tableData'], key=lambda x: x[2])
        if includeTags:
            mdata = [{title: val[i] for i, title in enumerate(self.__data__[
                'tableHeaders'])} for val in mdata]
        return mdata

    def getRanking(self, includeTags=False):
        mdata = self.getRows()
        users = self.getUsers()
        tmp = {x: list(
                        map(lambda k: k[1:],
                            list(
                                filter(lambda k: k[0] == x, mdata)
                                )
                            )
                ) for x in users
               }
        ret = []
        for k, x in tmp.items():
            for dif in range(1, 4):
                ret.append([
                    k,
                    sum(map(lambda w: w[3], filter(lambda s: s[0] == dif, x))),
                    dif,
                    sum(map(lambda w: w[1], filter(lambda s: s[0] == dif, x)))]
                )
        return list(filter(lambda x: x[3] > 0, ret))

    def getUsers(self):
        return self.__data__['users']

    def getDefaultUser(self):
        return self.__pref__['defaultUser']

    def setDefaultUser(self, user):
        if user in self.__data__['users']:
            self.__pref__['defaultUser'] = user

    def appendUser(self, user):
        if user not in self.__data__['users']:
            self.__data__['users'].append()
            self.__pref__['defaultUser'] = user
            self.__update__()

    def appendEntry(self, entry):
        ret = False
        if self.__validEntry__(entry):
            self.__data__['tableData'].append(self.__prepareEntry__(entry))
            ret = True
        if ret:
            writeJSON(self.__dataDir__, self.__data__)

    def __update__(self):
        writeJSON(self.__dataDir__, self.__data__)
        writeJSON(self.__prefDir__, self.__pref__)

    def __prepareEntry__(self, entry):
        if self.__validEntry__(entry):
            obj = list(entry.values()) if type(entry) == dict else entry
            obj[1] = {'Fácil': 1, 'Normal': 2, 'Difícil': 3}[obj[1]]
            if len(entry) == 7:
                obj.insert(4, len(obj[3]) if type(obj[3]) == list else 0)
            return obj

    def __validEntry__(self, entry):
        return type(entry) in (dict, list) and len(entry) in (7, 8)


if __name__ == '__main__':
    stg = dbGame(dataDir='../data/data.json', prefDir='../data/pref.json')
    print(stg.getRanking())
