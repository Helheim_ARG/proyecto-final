from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QIcon
from PyQt5 import uic
import json
from playsound import playsound
from time import sleep, time
if __name__ == '__main__':
    from objs import Key
else:
    from packages.objs import Key
from PyQt5.QtMultimedia import QSound
repl = {
    'a': ('a', 'á'),
    'e': ('e', 'é'),
    'i': ('i', 'í'),
    'o': ('o', 'ó'),
    'u': ('u', 'ú', 'ü')
}
colors = {'red': '#B03634', 'green': '#1FA831', 'gray': '#A2A2A2'}


def fontSize(lenght):
    if 1 <= lenght < 9:
        ret = 70
    elif 9 <= lenght < 10:
        ret = 65
    else:
        ret = 70
    return f'{ret}px'


def insertLetter(letter):
    letter = letter.lower()
    if letter in repl.keys():
        ret = ''.join(repl[letter])
    elif letter == 'nn':
        ret = 'ñ'
    else:
        ret = letter
    return ret


def fixAcentos(word):
    for k, v in repl.items():
        for x in v:
            word = word.replace(x, k)
    return word


class GameWindow(QDialog):
    def __init__(self, secretWords=[''], layoutUI='./src/layouts/game.ui'):
        super().__init__()
        uic.loadUi(layoutUI, self)
        self.setWindowIcon(QIcon('./src/img/icon.png'))
        self.sounds = {
            'success': QSound('./src/sounds/click_success1_1.wav', self),
            'failed': QSound('./src/sounds/click_failed.wav', self),
            'submit': QSound('./src/sounds/arpeggio-467_1.wav', self),
            'lose': QSound('./src/sounds/rubber-wall-69_1.wav', self)
        }
        # settaer palabra secreta
        self.startTime = int(time())
        secretWords = secretWords if type(
            secretWords) == list else [secretWords]
        self.__wordsList__ = secretWords
        self.__secretWord__ = secretWords.pop(0).lower()
        self.__playedWords__ = []
        self.ingresed = ''
        self.lives = 3
        self.score = 100
        self.updateDisplay()
        self.input.returnPressed.connect(self.intent)
        self.submit.clicked.connect(self.intent)
        # Instanciar keys
        k = list(map(lambda x: chr(x), list(range(97, 123))))
        k.insert(14, 'nn')  # Letra ñ
        self.__alphabetListIter__ = k
        c = 0
        for x in range(33):
            if x not in (11, 21, 22, 23, 32, 31):
                tmp = Key(k[c], f'key{k[c].upper()}')
                exec(f'self.key{k[c].upper()} = tmp')
                self.keyboard.addWidget(tmp, int(x/11), x % 11)
                c += 1
        for x in k:
            exec(
                f'self.key{x.upper()}.clicked.connect(lambda: self.run(\'{x}\'))',
                {"self": self}
                )

    def run(self, letter):
        letter = letter
        cond = letter.replace('nn', 'ñ') in fixAcentos(
            self.__secretWord__.lower())
        exec(f'self.key{letter.upper()}.onKeyPress({cond})')
        if letter.replace('nn', 'ñ') not in self.ingresed:
            self.ingresed += insertLetter(letter)
            self.score -= 0 if cond else 33.33
            self.lives -= 0 if cond else 1
            if cond:
                self.score += 1
            # playsound(
            #     f'./src/sounds/{["click_failed", "click_success1"][cond]}.mp3')
            self.sounds[('failed', 'success')[cond]].play()
            self.verify()

    def intent(self):
        inte = self.input.text().lower()
        # print(f'Ingresed: {inte.upper()}')
        if inte != '' and (inte == self.__secretWord__.lower() or inte == fixAcentos(self.__secretWord__.lower())):
            self.ingresed = self.__secretWord__
        else:
            if inte != '':
                self.lives -= 1
                self.sounds['failed'].play()
        self.input.clear()
        self.verify()

    def updateScore(self):
        self.scoreCounter.setText(f'SCORE:{str(int(self.score)).rjust(4)}')
        self.scoreCounter.repaint()

    def updateLives(self):
        if self.lives in range(1, 4):
            self.lifeCounter.setStyleSheet(f'''
            background-image: url(./src/img/{self.lives}.png);
            background-color: transparent;
            background-repeat: no-repeat;'''
                                           )
        else:
            self.updateDisplay(
                color='red',
                forceText=' '.join(self.__secretWord__).upper()
            )
            self.sounds['lose'].play()
            playsound('./src/sounds/rubber-wall-69.mp3')
            sleep(2)
            self.close()

    def updateDisplay(self, color='', forceText=''):
        ret = ''
        for x in self.__secretWord__:
            ret += ("_", x)[x in self.ingresed]
        ret = forceText if forceText != '' else ' '.join(ret).upper()
        self.textDisplay.setText(ret if ret != '' else 'N/W')
        if color != '' and color.lower() in colors.keys():
            self.textDisplay.setStyleSheet(f'''
            font-size: 70px;
            text-align: center;
            color: {colors[color.lower()]};
            background-color: transparent;
            ''')
        self.textDisplay.repaint()

    def reset(self, hard=False, newData=[]):
        for x in self.__alphabetListIter__:
            exec(f'self.key{x.upper()}.reset()')
        self.ingresed = ''
        self.updateDisplay(color='gray')
        if hard:
            self.score = 100
            self.lives = 3
            self.startTime = int(time())
            self.__playedWords__ = []
        if type(newData) == list and newData != []:
            self.__wordsList__ = newData
        self.updateLives()

    def submitGame(self):
        ret = self.lives >= 1
        for x in self.__secretWord__:
            ret = ret and x in self.ingresed
        if ret:
            playsound('./src/sounds/arpeggio-467.mp3')
            # sounds['submit'].play()
        return ret

    def loadNewWord(self):
        if len(self.__wordsList__) >= 1:
            self.__playedWords__.append(self.__secretWord__)
            self.__secretWord__ = self.__wordsList__.pop(0).lower()
            self.score += 100
        else:
            self.__playedWords__.append(self.__secretWord__)
            self.close()
        self.reset()
        self.updateDisplay()
        self.updateScore()

    def verify(self):
        self.updateScore()
        self.updateLives()
        if self.submitGame():
            self.updateDisplay(
                forceText=' '.join(self.__secretWord__).upper(),
                color='green'
            )
            sleep(2)
            self.loadNewWord()
        else:
            self.updateDisplay()

    def resume(self):
        etime = int(time())
        return {
            'score': 0 if self.score == 100 else int(self.score),
            'words': self.__playedWords__,
            'startTime': self.startTime,
            'endTime': etime,
            'time': etime - self.startTime
        }

    def resumePlain(self):
        return json.dumps(self.resume(), ensure_ascii=False)


if __name__ == '__main__':
    print(insertLetter('nn'))
    print(fixAcentos('ñandú'))
