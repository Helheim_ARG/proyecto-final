from PyQt5.QtWidgets import QMainWindow, QDialog, QTableWidgetItem
from PyQt5.QtGui import QIcon
from PyQt5 import uic
from PyQt5 import QtCore
from PyQt5.QtGui import QCursor
from random import randint, sample
if __name__ == '__main__':
    from objs import Toggle
else:
    from packages.objs import Toggle, getCSS
    from PyQt5.QtWidgets import QApplication as AppRuntime
from packages.game import GameWindow
from packages.storagemanager import dbGame


class MainWindow(QMainWindow):
    def __init__(self, allData={}, layoutUI='./src/layouts/main.ui'):
        super().__init__()
        uic.loadUi(layoutUI, self)
        self.setWindowIcon(QIcon('./src/img/icon.png'))
        # instanciar db del juego
        self.storage = dbGame()
        # Entra la list de usuarios
        self.allWords = allData
        self.__allUsers__ = self.storage.getUsers()
        # Instanciar QDialog-Selector
        self.__selector__ = UserSelector(
            userList=self.__allUsers__
        )
        self.__selector__.setWindowModality(QtCore.Qt.ApplicationModal)
        # Instanciar QDialog-RankingTable
        self.__ranking__ = RankingTable(
            # Aqui se cargan los datos de la tabla como una matriz
            data=self.storage.getRanking()
        )
        self.__ranking__.setWindowModality(QtCore.Qt.ApplicationModal)
        # Usuario default
        self.defaultUser = ''
        # Instanciar Toggles
        self.toggleCat = Toggle(
            title='Categoria',
            options=list(self.allWords.keys())
        )
        self.toggleDifc = Toggle(
            title='Modo',
            options={'Fácil': 1, 'Normal': 2, 'Difícil': 3}
        )
        # Agregarlos al Layout
        self.togglesContainer.addWidget(self.toggleCat)
        self.togglesContainer.addWidget(self.toggleDifc)
        # Boton SelectUser
        self.__verifyBtnUserList__()
        self.selectUser.clicked.connect(self.openSelector)
        # Boton Ranking
        self.openRanking.clicked.connect(self.showRanking)
        # set default User
        if self.defaultUser == '':
            self.inputUser.setText(f'userGuest{randint(999, 9999)}')
        # Run
        self.playGame.clicked.connect(self.runGame)

    def openSelector(self):
        if len(self.__allUsers__) >= 1:
            self.setDisabled(True)
            self.__selector__.exec()
            self.setDisabled(False)
            self.inputUser.setText(self.__selector__.getValue())

    def showRanking(self, newData=[]):
        newData = newData if type(newData) == list else []
        if newData != []:
            self.__ranking__.changeData(newData)  # Data Actualizada
        self.__ranking__.exec()

    def __verifyBtnUserList__(self):
        cond = len(self.__allUsers__) >= 1
        self.selectUser.setStyleSheet(getCSS(
            borderStyle='none',
            width='35px',
            height='36px',
            backgroundColor='transparent',
            backgroundImage=f'url(./src/img/userList{("Off", "On")[cond]}.png)',
            backgroundRepeat='no-repeat',
            backgroundPosition='center center'
        ))
        self.selectUser.setCursor(
            QCursor((QtCore.Qt.ArrowCursor, QtCore.Qt.PointingHandCursor)[cond]))

    def runGame(self):
        cat = self.toggleCat.getValue()
        diffic = self.toggleDifc.getValue()
        diffic_ref = ['Fácil', 'Normal', 'Difícil']
        kd = int(len(self.allWords[cat])/3)
        words = sample(
            sorted(
                self.allWords[cat],
                key=lambda x: len(x)
            )[(diffic - 1) * kd:diffic * kd], kd
        )
        user = self.inputUser.text()
        self.defaultUser = user
        game = GameWindow(secretWords=words)
        game.setWindowModality(QtCore.Qt.ApplicationModal)
        if user not in self.__allUsers__:
            self.__selector__.append(user)
        game.exec()
        print({'player': self.defaultUser,
               'dificultad': diffic_ref[diffic-1], **game.resume()})
        self.storage.appendEntry({'player': self.defaultUser,
                                  'dificultad': diffic_ref[diffic-1], **game.resume()})
        self.showRanking(newData=self.storage.getRanking())


class UserSelector(QDialog):
    def __init__(self, userList=[], layoutUI='./src/layouts/selectUser.ui'):
        super().__init__()
        uic.loadUi(layoutUI, self)
        self.setWindowIcon(QIcon('./src/img/icon.png'))
        self.submit.clicked.connect(self.onClick)
        self.userList = userList
        for x in userList:
            self.usersList.addItem(x[0].lower()+x[1:])
        self.__value__ = self.usersList.currentItem()

    def getValue(self):
        return self.__value__

    def append(self, user):
        if user not in self.userList and type(user) == str:
            self.userList.append(user)
            self.usersList.addItem(user[0].lower()+user[1:])

    def onClick(self):
        self.__value__ = self.usersList.item(
            self.usersList.currentRow()).text()
        self.close()


class RankingTable(QDialog):
    def __init__(self, data=[], layoutUI='./src/layouts/ranking.ui'):
        super().__init__()
        uic.loadUi(layoutUI, self)
        self.setWindowIcon(QIcon('./src/img/icon.png'))
        self.data = data
        self.loadData(data)

    def loadData(self, data=[]):
        for i, row in enumerate(data):
            self.dataTable.insertRow(i)
            for j, column in enumerate(row):
                cell = QTableWidgetItem(row[j])
                if j > 0:
                    cell.setData(QtCore.Qt.DisplayRole, row[j])
                self.dataTable.setItem(i, j, cell)

    def clear(self):
        self.dataTable.setRowCount(0)

    def changeData(self, newData):
        self.clear()
        self.loadData(newData)
